#!/usr/bin/env perl
#
# aristoteles - a regex-based, just-for-fun Italian-to-Portuguese translator
# Copyright (C) 2009 Davide Mancusi <arekfu@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use warnings;
use Tie::IxHash;

binmode(STDOUT, ":utf8");
binmode(DATA, ":utf8");

# Check the number of arguments
die "Usage: ", $0, " <filename>\n" unless $#ARGV == 0;

# Read the pattern dictionary
tie my %patdic, 'Tie::IxHash';

while( <DATA> ) {
	chomp;
	my ($search, $subst) = split /\t+/;
	$patdic{$search} = $subst;
}

# Open file to translate
open( my $infile, "<utf8", $ARGV[0] ) or die "Can't open input file: $!\n";

# Translate!
while( <$infile> ) {
	foreach my $search (keys %patdic) {
		eval "s/$search/$patdic{$search}/g";
		if( $search =~ /^\\b([[:lower:]].*)/ )
		{
			my $search2 = '\b'.ucfirst($1);
			my $subst2 = ucfirst($patdic{$search});
			eval "s/$search2/$subst2/g";
		}
	}
	print;
}

__END__
\bdove\b	onde
acqua\b	agua
\blatte\b	leite
mezzo	meio
mezza	meia
\bche\b	que
\bchi\b	quem
\bè\b	é
\be'\b	é
\bho\b	tenho
\bhai?\b	tem
\babbiamo\b	tem
\bavete\b	tem
\bhanno\b	tem
\bcos(ì|i')\b	assim
\bperch(é|e')\b	porque
\bbuon(o)?\b	bom
\bbuona\b	boa
\bdestra\b	direita
\bsinistra\b	esquerda
\btutto\b	todo
\btutta\b	toda
\btutti\b	todos
\btutte\b	todas
\bquesto\b	este
\bquesta\b	esta
\bquel(lo)?\b	esse
\bquella?\b	essa
\bil\b	o
\blo\b	o
\bl'([[:alpha:]]+)(a|i|u)\b	a $1$2
\bl'([[:alpha:]]+)(o|e)\b	o $1$2
\bla\b		a
\bi\b	os
\bgli\b	os
\ble\b		as
\b(di|a|da|in|con|su|per|fra|tra)\Wme\b	$1 mim
\b(di|a|da|in|con|su|per|fra|tra)\Wte\b	$1 ti
\bal(lo)?\b	ao
\balla\b	à
\ba(i|gli)\b	aos
\balle\b	às
\ball'([[:alpha:]]+)(a|i)\b	à $1$2
\ball'([[:alpha:]]+)(o|e)\b	ao $1$2
\bdal(lo)?\b	do
\bdalla\b	da
\bda(i|gli)\b	dos
\bdalle\b	das
\bda\b		de
\bdall'([[:alpha:]]+)(a|i)\b	da $1$2
\bdall'([[:alpha:]]+)(o|e|u)\b	do $1$2
\bsul(lo)?\b	sobre o
\bsulla\b	sobre a
\bsu(i|gli)\b	sobre os
\bsulle\b	sobre as
\bsu\b		sobre
\bdell'([[:alpha:]]+)(a|i)\b	da $1$2
\bdell'([[:alpha:]]+)(o|e|u)\b	do $1$2
\bdel(lo)?\b	do
\bdella\b	da
\bde(i|gli)\b	dos
\bdelle\b	das
\bdi\b		de
\bdell'([[:alpha:]]+)(a|i)\b	da $1$2
\bdell'([[:alpha:]]+)(o|e|u)\b	do $1$2
\bnel(lo)?\b	no
\bnella\b	na
\bne(i|gli)\b	nos
\bnelle\b	nas
\bnell'([[:alpha:]]+)(a|i)\b	à $1$2
\bnell'([[:alpha:]]+)(o|e)\b	ao $1$2
\bcon\b		com
\bcome\b		como
\bsotto\b	debaixo
\bnon?\b	não
\bun'	uma 
\buno?\b	um
\buna\b		uma
\bdue\b		dois
\btre\b		três
\bquattro\b	quatro
\bcinque\b	cinco
\bsei\b	seis
\bsette\b	sete
otto\b	oito
\bdieci\b	dez
\bundici\b	onze
\bdodici\b	doze
\btredici\b	treze
\bquattordici\b	catorze
\bquindici\b	quinze
\bsedici\b	dezasseis
\bdiciassette\b	dezassete
\bdiciotto\b	dezoito
\bdiciannove\b	dezanove
\bper\b		para
\bin\b		em
\bpi(ù|u')\b	mais
\bma\b		mas
ssione\b	ção
ssioni\b	ções
sione\b	ção
sioni\b	ções
zione\b	ção
zioni\b	ções
ine\b	inhas
one\b	ão
ini\b	inhos
oni\b	ãos
ina\b	inha
ona\b	oa
ino\b	inho
one\b	oas
ico\b	igo
(b|c|d|f|g|l|m|n|p|q|t|v|z)\1	$1
([^sl[:^alpha:]])ce\b	$1z
sc(ia|e|i|io|iu)	c$1
gli?(a|e|i|o|u)	lh$1
gni?(a|e|i|o|u)	nh$1
cchi	lh
\bpi(a|e|o|u)	ch$1
\bs(t|p|d|f|c|b)	es$1
(a|e|i|o)(r|l)e\b	$1$2
zi(a|e|o|u)	ci$1
ol(o|a)\b	ul$1
at(o|a)\b	ad$1
tà\b	tade
ta'\b	tade
(a|e)nza\b	$1nça
(a|e)nze\b	$1nças
che\b	cas
chi\b	cos
uo\w	o
ie\w	e
